import { Dropbox } from 'dropbox';

const accessToken = '4qDvoZRQa-AAAAAAAAAADzGQ0hB-Cr0EPJBMQdAN10mwlmIIPAH3zbOX79_ojWkC';

const dbx = new Dropbox({
    accessToken,
    fetch
});

function upload() {
    let content = $('.uploadFile').change().get(0)[0];
    let docName = $('.uploadFile').change().val().toString();
    docName = docName.replace("C:\\fakepath\\", "");
    dbx.filesUpload({
        contents: content,
        path: '/Images/' + docName,
        mode: {
            update: 'aaaa00002',
            '.tag': 'update'
        }
    }).then(response => dbx.filesListFolder({
        path: '/Images'
    }).then(response => {
        $(".allFiles").empty();
        response.entries.forEach(element => {
            $(".allFiles").append("<p>" + element.name + '</p>');
        }
        )
    }));


}

function deleteFile(fileToDelete) {
    dbx.filesDelete({
        path: "/Images/" + fileToDelete
    }).then(response => dbx.filesListFolder({
        path: '/Images'
    }).then(response => {
        $(".allFiles").empty();
        response.entries.forEach(element => {
            $(".allFiles").append("<p>" + element.name + '</p>');
        }
        )
    }));

}


var lat = 49.1191;
var lon = 6.1727;
var macarte = null;
function initMap() {
    macarte = L.map('map').setView([lat, lon], 11);
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);
    var marker = L.marker([lat, lon]).addTo(macarte);
}
window.onload = function () {
    initMap();
    dbx.filesListFolder({
        path: '/Images'
    }).then(response => {
        $(".allFiles").empty();
        response.entries.forEach(element => {
            $(".allFiles").append("<p>" + element.name + '</p>');
        })
    });
    dbx.sharingCreateSharedLink({
        path: "/Images",
        short_url: true
    }).then(response => $(".link").append("<a href='" + response.url + "'>" + response.url + "<a>"));

};

$('#upload').click(() => upload());
$('#delete').click(() => deleteFile($('#deleteFile').val().toString()));

